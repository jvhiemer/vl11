/**
 * authenticationService
 **/
angular.module('services')
  .factory('authenticationService', function authenticationService($rootScope, $state, $http, $interval,
      clientCacheService) {
    var authentication = {};
    var timeout = 10 * 60 * 1000;
    var anonymousStates = ['register', 'logout'];

    authentication.authenticate = function(callback) {

    }

    authentication.login = function(user) {

    }

    authentication.logout = function() {

    }

    authentication.isAuthenticated = function() {

    }

    $rootScope.$on('$destroy', function() {
       $interval.cancel(autoReAuthentication);
     });

    function getToken(user) {
      if (user != undefined && user.password != undefined && user.username != null) {
        $rootScope.hideLoginForm = true;

        $http.defaults.headers.common['Authorization'] = 'bearer ' + user.token;

        var head = {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
            'Accept': 'application/json;charset=utf-8'
          }
        };

        $http.post(REST_API + '/login', data, head).success(function(data, status, headers, config) {

          $http.defaults.headers.common['Authorization'] = 'bearer ' + data.accessToken;
          $rootScope.isAuthenticated = true;

          user.token = data.accessToken;
          clientCacheService.storeUser(user);

          $state.go('home');
        }).error(function(data, status, headers) {
          responseService.error(data, "Invalid user credentials - or endpoint not reachable");
        });
      } else {
        $rootScope.hideLoginForm = false;

        clientCacheService.clearUser();
        $state.go('login');
      }
    }

    return authentication;
  });
