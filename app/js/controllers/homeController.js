/**
 * HomeController
 **/

angular.module('controllers')
  .controller('homeController',
    function HomeController($scope, $rootScope, authenticationService) {
      $rootScope.$on('$stateChangeSuccess',
        function(event, toState, toParams, fromState, fromParams) {
          $rootScope.lastPageChange = new Date().getTime();
          if (!authenticationService.isAuthenticated()) {
            authenticationService.logout();
          }
      });


      $scope.logout = function() {
        $rootScope.hideLoginForm = false;
        authenticationService.logout();
      };

      function isOpen(section) {
        return menu.isSectionSelected(section);
      }

      function toggleOpen(section) {
        menu.toggleSelectSection(section);
      }
    });
